export const isLoading = state => state.isLoading;
export const pageInfo = state => state.pageInfo;
export const vulnerabilities = state => state.vulnerabilities || [];

export default () => {};
