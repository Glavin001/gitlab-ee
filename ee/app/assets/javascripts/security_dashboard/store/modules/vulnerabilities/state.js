export default () => ({
  vulnerabilitiesUrl: false,
  vulnerabilities: [],
  pageInfo: {},
  isLoading: false,
});
